$(document).ready(function() {
  $(".gallery").slick({
    dots: true,
    draggable: true,
    responsive: true,
    mobileFirst: true
  });
});
